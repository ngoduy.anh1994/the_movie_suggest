package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.LayoutItemRvTvshowInListWatchFragmentBinding
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.main.ListWatchFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain

@SuppressLint("UseRequireInsteadOfGet")
class AdapterRvListWatchFragment(
    private val listWatchFragment: ListWatchFragment,
    private val interfacePassData: InterfacePassData
) : ListAdapter<TvShow, AdapterRvListWatchFragment.ItemTvShowListWatchViewHolder>(
    ItemTvShowListWatchDiffCallBack()
) {

    private val viewModelMain by lazy {
        ViewModelProvider(listWatchFragment,AppViewModelFactory(listWatchFragment.context!!))[ViewModelMain::class.java]
    }

    inner class ItemTvShowListWatchViewHolder(private val binding: LayoutItemRvTvshowInListWatchFragmentBinding,private val interfacePassData: InterfacePassData) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(tvShow: TvShow) {
            itemView.run {
                Glide.with(this).load(tvShow.image_thumbnail_path)
                    .into(binding.itemRvListWatchFragment.ivMovieItemRvInBodyHomeFra)
                binding.itemRvListWatchFragment.tvNameMovieItemRvInBodyHomeFra.text = tvShow.name
                binding.itemRvListWatchFragment.tvCountryItemRvInBodyHomeFra.text = tvShow.country
                binding.itemRvListWatchFragment.tvReleaseDateItemRvInBodyHomeFra.text =
                    tvShow.start_date
                if (tvShow.save_movie) {
                    binding.itemRvListWatchFragment.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                } else {
                    binding.itemRvListWatchFragment.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                }
                binding.itemRvListWatchFragment.ivSaveItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.save_movie = !tvShow.save_movie
                    if (tvShow.save_movie) {
                        binding.itemRvListWatchFragment.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                    } else {
                        binding.itemRvListWatchFragment.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                        viewModelMain.deleteTvShow(tvShow)
                    }
                }

                if (tvShow.rate_movie) {
                    binding.itemRvListWatchFragment.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                } else {
                    binding.itemRvListWatchFragment.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                }
                binding.itemRvListWatchFragment.ivStarItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.rate_movie = !tvShow.rate_movie
                    if (tvShow.rate_movie) {
                        binding.itemRvListWatchFragment.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                    } else {
                        binding.itemRvListWatchFragment.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                    }
                    if (tvShow.save_movie){
                        viewModelMain.insertTvShow(tvShow)
                    }
                }
                setOnClickListener {
                    interfacePassData.passDataMovie(tvShow)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemTvShowListWatchViewHolder {
        return ItemTvShowListWatchViewHolder(
            LayoutItemRvTvshowInListWatchFragmentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),interfacePassData
        )
    }

    override fun onBindViewHolder(holder: ItemTvShowListWatchViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }
}

class ItemTvShowListWatchDiffCallBack : DiffUtil.ItemCallback<TvShow>() {
    override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem.save_movie == oldItem.save_movie && newItem.rate_movie == newItem.rate_movie
    }

}