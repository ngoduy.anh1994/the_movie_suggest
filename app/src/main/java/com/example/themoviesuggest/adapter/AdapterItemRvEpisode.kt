package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviesuggest.databinding.LayoutItemDataEpisodeInRvInEpisodeFragmentBinding
import com.example.themoviesuggest.model.detail.Episode

class AdapterItemRvEpisode: ListAdapter<Episode,AdapterItemRvEpisode.ViewHolderItemRvEpisode>(DiffCallBackItemRvEpisode()) {
    inner class ViewHolderItemRvEpisode(private val binding: LayoutItemDataEpisodeInRvInEpisodeFragmentBinding) : RecyclerView.ViewHolder(binding.root){
        @SuppressLint("SetTextI18n")
        fun onBind(episode: Episode){
            itemView.run {
                binding.tvItemEpisode.text = "Episode: " + episode.episode.toString() + " - Season: "+ episode.season.toString()
                binding.tvItemNameEpisode.text = "Name: " + episode.name
                binding.tvItemAirDateEpisode.text = "Start date: " + episode.air_date
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderItemRvEpisode {
        return ViewHolderItemRvEpisode(
            LayoutItemDataEpisodeInRvInEpisodeFragmentBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolderItemRvEpisode, position: Int) {
        holder.onBind(getItem(position))
    }
}
class DiffCallBackItemRvEpisode : DiffUtil.ItemCallback<Episode>(){
    override fun areItemsTheSame(oldItem: Episode, newItem: Episode): Boolean {
        return newItem.name == oldItem.name
    }

    override fun areContentsTheSame(oldItem: Episode, newItem: Episode): Boolean {
        return newItem == oldItem
    }

}