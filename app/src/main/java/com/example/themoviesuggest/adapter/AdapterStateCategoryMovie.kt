package com.example.themoviesuggest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.LayoutItemRvStateMovieBinding
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.main.CategoryFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain

class AdapterStateCategoryMovie(categoryFragment: CategoryFragment,private val interfacePassData: InterfacePassData) : ListAdapter<TvShow,AdapterStateCategoryMovie.ViewHolderStateMovie>(DiffCallBackStateMovie()) {
    private val viewModelMain by lazy {
        ViewModelProvider(categoryFragment,AppViewModelFactory(categoryFragment.requireContext()))[ViewModelMain::class.java]
    }
    inner class ViewHolderStateMovie(private val binding: LayoutItemRvStateMovieBinding,private val interfacePassData: InterfacePassData): RecyclerView.ViewHolder(binding.root){
        fun onBind(tvShow: TvShow){
            itemView.run {
                Glide.with(this).load(tvShow.image_thumbnail_path).into(binding.itemInRvCategoryFra.ivMovieItemRvInBodyHomeFra)
                binding.itemInRvCategoryFra.tvNameMovieItemRvInBodyHomeFra.text = tvShow.name
                binding.itemInRvCategoryFra.tvCountryItemRvInBodyHomeFra.text = tvShow.country
                binding.itemInRvCategoryFra.tvReleaseDateItemRvInBodyHomeFra.text = tvShow.start_date
                if (tvShow.save_movie){
                    binding.itemInRvCategoryFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                }else{
                    binding.itemInRvCategoryFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                }
                binding.itemInRvCategoryFra.ivSaveItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.save_movie = !tvShow.save_movie
                    if (tvShow.save_movie){
                        binding.itemInRvCategoryFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                        viewModelMain.insertTvShow(tvShow)
                    }else{
                        binding.itemInRvCategoryFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                        viewModelMain.deleteTvShow(tvShow)
                    }
                }

                if (tvShow.rate_movie){
                    binding.itemInRvCategoryFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                }else{
                    binding.itemInRvCategoryFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                }
                binding.itemInRvCategoryFra.ivStarItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.rate_movie = !tvShow.rate_movie
                    if (tvShow.rate_movie){
                        binding.itemInRvCategoryFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                    }else{
                        binding.itemInRvCategoryFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                    }
                    if (tvShow.save_movie){
                        viewModelMain.insertTvShow(tvShow)
                    }
                }
                setOnClickListener {
                    interfacePassData.passDataMovie(tvShow)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolderStateMovie, position: Int) {
        holder.onBind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderStateMovie {
       return ViewHolderStateMovie(
           LayoutItemRvStateMovieBinding.inflate(LayoutInflater.from(parent.context),parent,false),interfacePassData
       )
    }
}
class DiffCallBackStateMovie: DiffUtil.ItemCallback<TvShow>(){
    override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem == oldItem
    }

    override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem == oldItem
    }

}