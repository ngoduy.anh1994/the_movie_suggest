package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviesuggest.databinding.LayoutItemRvInItemInformationMovieInRvFragmentDetailBinding

class AdapterItemRvGenresInRvInformationInDetailFragment : RecyclerView.Adapter<AdapterItemRvGenresInRvInformationInDetailFragment.ViewHolderItemRvGenresInRvInformationInDetailFragment>()  {
    private var listDataGeneres = ArrayList<String>()
    @SuppressLint("NotifyDataSetChanged")
    fun receiverData(listDataGeneres : ArrayList<String>){
        this.listDataGeneres =listDataGeneres
        notifyDataSetChanged()
    }
    inner class ViewHolderItemRvGenresInRvInformationInDetailFragment(private val binding : LayoutItemRvInItemInformationMovieInRvFragmentDetailBinding) : RecyclerView.ViewHolder(binding.root){
        fun onBind(genres: String){
            itemView.run {
                binding.tvCategoryItemInformationMovieInRvDetailFra.text = genres
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderItemRvGenresInRvInformationInDetailFragment {
        return ViewHolderItemRvGenresInRvInformationInDetailFragment(
            LayoutItemRvInItemInformationMovieInRvFragmentDetailBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(
        holder: ViewHolderItemRvGenresInRvInformationInDetailFragment,
        position: Int
    ) {
        holder.onBind(listDataGeneres[position])
    }

    override fun getItemCount(): Int = listDataGeneres.size
}