package com.example.themoviesuggest.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class AdapterViewpagerItemDetailMovieInRvDetailFragment(fm: FragmentManager,lifecycle: Lifecycle,private val listFragment: ArrayList<Fragment>) :
    FragmentStateAdapter(fm,lifecycle) {
    override fun getItemCount(): Int = listFragment.size
    override fun createFragment(position: Int): Fragment = listFragment[position]
}