package com.example.themoviesuggest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.databinding.LayoutItemOfRvAsBannerHomeFraBinding
import com.example.themoviesuggest.model.popular.TvShow

class AdapterItemRVAsBannerInRVHomeFra(private val interfacePassData: InterfacePassData) : ListAdapter<TvShow,ViewHolderItemRVAsBannerInRVHomeFra>(DiffCallBackItemRVAsBannerInRVHomeFra()) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderItemRVAsBannerInRVHomeFra {
        return ViewHolderItemRVAsBannerInRVHomeFra(
            LayoutItemOfRvAsBannerHomeFraBinding.inflate(LayoutInflater.from(parent.context),parent,false),interfacePassData
        )
    }

    override fun onBindViewHolder(holder: ViewHolderItemRVAsBannerInRVHomeFra, position: Int) {
        holder.onBind(getItem(position))
    }
}
class ViewHolderItemRVAsBannerInRVHomeFra(private val binding : LayoutItemOfRvAsBannerHomeFraBinding,private val interfacePassData: InterfacePassData) : RecyclerView.ViewHolder(binding.root){
    fun onBind(tvShow: TvShow){
        itemView.run {
            Glide.with(this).load(tvShow.image_thumbnail_path).into(binding.ivItemOfRvBanner)
            setOnClickListener {
                interfacePassData.passDataMovie(tvShow)
            }
        }
    }
}
class DiffCallBackItemRVAsBannerInRVHomeFra : DiffUtil.ItemCallback<TvShow>(){
    override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem == oldItem
    }

    override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem == oldItem
    }

}