package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.LayoutItemRvInBottomSheetDialogFragmentOfPageBinding
import com.example.themoviesuggest.interface_app.InterfacePage
import com.google.android.material.bottomsheet.BottomSheetDialog

class AdapterRvPageInBottomSheetDialog(
    private val interfacePage: InterfacePage,
    private val bottomSheet: BottomSheetDialog
) :
    ListAdapter<SelectPage, AdapterRvPageInBottomSheetDialog.ViewHolderSelectPage>(
        DiffCallBackSelectPage()
    ) {
    private var page = 0
    fun getPageCurrent(page: Int){
        this.page = page
        notifyDataSetChanged()
    }
    inner class ViewHolderSelectPage(private val binding: LayoutItemRvInBottomSheetDialogFragmentOfPageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun onBind(selectPage: SelectPage) {
            itemView.run {
                binding.tvItemRvOfPage.text = "Page:    ${selectPage.page}"
                selectPage.selectPage = selectPage.page == page
                setOnClickListener {
                    for (i in 0 until currentList.size) {
                        if (currentList[i] != selectPage && currentList[i].selectPage) {
                            currentList[i].selectPage = false
                        }
                        selectPage.selectPage = true
                        notifyDataSetChanged()
                    }
                    interfacePage.getPage(selectPage)
                    bottomSheet.dismiss()
                }
                if (selectPage.selectPage) {
                    binding.ivItemSelectPage.setImageResource(R.drawable.ic_select_page)
                } else {
                    binding.ivItemSelectPage.setImageResource(R.drawable.ic_un_select_page)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSelectPage {
        return ViewHolderSelectPage(
            LayoutItemRvInBottomSheetDialogFragmentOfPageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolderSelectPage, position: Int) {
        holder.onBind(getItem(position))
    }
}

class DiffCallBackSelectPage : DiffUtil.ItemCallback<SelectPage>() {
    override fun areItemsTheSame(oldItem: SelectPage, newItem: SelectPage): Boolean {
        return newItem.page == oldItem.page
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: SelectPage, newItem: SelectPage): Boolean {
        return newItem == oldItem
    }

}

class SelectPage(
    var page: Int = 1,
    var selectPage: Boolean = false
)