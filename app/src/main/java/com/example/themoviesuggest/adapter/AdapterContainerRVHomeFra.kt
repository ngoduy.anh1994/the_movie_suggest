package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.R
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.LayoutBodyInRvOfHomeFraBinding
import com.example.themoviesuggest.databinding.LayoutRvAsBannerInRvOfHomeFraBinding
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.util.Util
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlin.collections.ArrayList

class AdapterContainerRVHomeFra(
    private val interfacePassData: InterfacePassData,
    private val fmActivity: FragmentActivity
) :
    ListAdapter<ContainerDataMovieHomeFra, AdapterContainerRVHomeFra.ViewHolderContainerDataMovieHomeFra>(
        DiffCallBackContainerDataMovieHomeFra()
    ) {
    private val mAdapterItemBodyInRvOfHomeFra =
        AdapterItemBodyInRvOfHomeFra(interfacePassData, fmActivity)
    private var mListDataBodyMovie = ArrayList<TvShow>()
    private var listDataMovieRunning: List<TvShow> = ArrayList<TvShow>()
    private var listDataMovieEnded: List<TvShow> = ArrayList<TvShow>()
    private val mAdapterItemRVAsBannerInRVHomeFra =
        AdapterItemRVAsBannerInRVHomeFra(interfacePassData)
    private var mListDataMovieBanner = ArrayList<TvShow>()
    private lateinit var tvSelectRunning: TextView
    private lateinit var tvSelectEnded: TextView
    private lateinit var dialogSelectStateMovie: Dialog
    private lateinit var ivClose: ImageView

    private val viewModel by lazy {
        ViewModelProvider(fmActivity, AppViewModelFactory(fmActivity))[ViewModelMain::class.java]
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderContainerDataMovieHomeFra {
        return when (viewType) {
            ConstantOfApp.TYPE_DATA_MOVIE_AS_BANNER -> ViewHolderItemRVBanner(
                LayoutRvAsBannerInRvOfHomeFraBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
            ConstantOfApp.TYPE_DATA_MOVIE_BODY -> ViewHolderItemRVBody(
                LayoutBodyInRvOfHomeFraBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ), fmActivity
            )
            else -> ViewHolderItemRVBody(
                LayoutBodyInRvOfHomeFraBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ), fmActivity
            )
        }
    }

    override fun onBindViewHolder(holder: ViewHolderContainerDataMovieHomeFra, position: Int) {
        holder.onBind(getItem(position), position)
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).type
    }

    abstract class ViewHolderContainerDataMovieHomeFra(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun onBind(containerDataMovieHomeFra: ContainerDataMovieHomeFra, position: Int)
    }

    inner class ViewHolderItemRVBanner(
        private val binding: LayoutRvAsBannerInRvOfHomeFraBinding,
    ) :
        ViewHolderContainerDataMovieHomeFra(binding.root) {
        override fun onBind(containerDataMovieHomeFra: ContainerDataMovieHomeFra, position: Int) {
            itemView.run {
                binding.rvAsBannerInHomeFra.adapter = mAdapterItemRVAsBannerInRVHomeFra
                binding.rvAsBannerInHomeFra.layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                addData(containerDataMovieHomeFra)
            }
        }

        private fun addData(containerDataMovieHomeFra: ContainerDataMovieHomeFra) {
            itemView.run {
                mListDataMovieBanner = containerDataMovieHomeFra.mListDataBannerMovie
                mAdapterItemRVAsBannerInRVHomeFra.submitList(mListDataMovieBanner)
            }
        }

    }

    inner class ViewHolderItemRVBody(
        private val binding: LayoutBodyInRvOfHomeFraBinding,
        private val fmActivity: FragmentActivity
    ) :
        ViewHolderContainerDataMovieHomeFra(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onBind(containerDataMovieHomeFra: ContainerDataMovieHomeFra, position: Int) {
            itemView.run {
                //init recycler view
                binding.rvInRvBodyHomeFra.adapter = mAdapterItemBodyInRvOfHomeFra
                binding.rvInRvBodyHomeFra.layoutManager = LinearLayoutManager(context)
                viewModel.apply {
                    getDataTvShow()
                    dataListTvShow.onEach { listTvShow ->
                        if (listTvShow.isNotEmpty()) {
                            for (j in 0 until containerDataMovieHomeFra.mListDataBodyMovie.size) {
                                for (i in listTvShow.indices) {
                                    if (containerDataMovieHomeFra.mListDataBodyMovie[j].id == listTvShow[i].id) {
                                        containerDataMovieHomeFra.mListDataBodyMovie[j].save_movie = listTvShow[i].save_movie
                                        containerDataMovieHomeFra.mListDataBodyMovie[j].rate_movie = listTvShow[i].rate_movie
                                    }
                                }
                                mAdapterItemBodyInRvOfHomeFra.notifyDataSetChanged()
                            }
                        } else {
                            for (i in 0 until containerDataMovieHomeFra.mListDataBodyMovie.size) {
                                containerDataMovieHomeFra.mListDataBodyMovie[i].save_movie =
                                    false
                                containerDataMovieHomeFra.mListDataBodyMovie[i].rate_movie =
                                    false
                            }
                        }
                        mAdapterItemBodyInRvOfHomeFra.notifyDataSetChanged()
                    }.launchIn(fmActivity.lifecycleScope)
                }
                mListDataBodyMovie = containerDataMovieHomeFra.mListDataBodyMovie
                dialogSelectStateMovie = Util.createDialog(
                    R.layout.layout_dialog_select_state_movie_running_or_ended,
                    context,
                    Gravity.BOTTOM
                )
                tvSelectRunning = dialogSelectStateMovie.findViewById(R.id.tv_select_running)
                tvSelectEnded = dialogSelectStateMovie.findViewById(R.id.tv_select_ended)
                ivClose = dialogSelectStateMovie.findViewById(R.id.iv_close_dialog)
                //default
                hideOrShowUnderline(
                    binding.tvRunningItemInRvBodyHomeFra,
                    binding.viewUnderLineOfTvRunning,
                    binding.tvEndedItemInRvBodyHomeFra,
                    binding.viewUnderLineOfTvEnded,
                    context
                )
                listDataMovieRunning = mListDataBodyMovie.filter { tvShow ->
                    tvShow.status == ConstantOfApp.STATUS_RUNNING
                }
                mAdapterItemBodyInRvOfHomeFra.submitList(listDataMovieRunning)
                // event
                binding.tvRunningItemInRvBodyHomeFra.setOnClickListener {
                    hideOrShowUnderline(
                        binding.tvRunningItemInRvBodyHomeFra,
                        binding.viewUnderLineOfTvRunning,
                        binding.tvEndedItemInRvBodyHomeFra,
                        binding.viewUnderLineOfTvEnded,
                        context
                    )
                    listDataMovieRunning = mListDataBodyMovie.filter { tvShow ->
                        tvShow.status == ConstantOfApp.STATUS_RUNNING
                    }
                    mAdapterItemBodyInRvOfHomeFra.submitList(listDataMovieRunning)
                }

                binding.tvEndedItemInRvBodyHomeFra.setOnClickListener {
                    hideOrShowUnderline(
                        binding.tvEndedItemInRvBodyHomeFra,
                        binding.viewUnderLineOfTvEnded,
                        binding.tvRunningItemInRvBodyHomeFra,
                        binding.viewUnderLineOfTvRunning,
                        context
                    )
                    listDataMovieEnded = mListDataBodyMovie.filter { tvShow ->
                        tvShow.status == ConstantOfApp.STATUS_ENDED
                    }
                    mAdapterItemBodyInRvOfHomeFra.submitList(listDataMovieEnded)
                }
                binding.tvMoreInRvBodyHomeFra.setOnClickListener {
                    dialogSelectStateMovie.show()
                    tvSelectRunning.setOnClickListener {
                        val bundle = Bundle()
                        bundle.putString(ConstantOfApp.STATE_MOVIE, "Running")
                        fmActivity.findNavController(R.id.nav_host_introduce)
                            .navigate(R.id.categoryFragment, bundle)
                        dialogSelectStateMovie.dismiss()
                    }
                    tvSelectEnded.setOnClickListener {
                        val bundle = Bundle()
                        bundle.putString(ConstantOfApp.STATE_MOVIE, "Ended")
                        fmActivity.findNavController(R.id.nav_host_introduce)
                            .navigate(R.id.categoryFragment, bundle)
                        dialogSelectStateMovie.dismiss()
                    }
                    ivClose.setOnClickListener {
                        dialogSelectStateMovie.dismiss()
                    }
                }
            }
        }

        @RequiresApi(Build.VERSION_CODES.M)
        private fun hideOrShowUnderline(
            tvOnClick: TextView,
            underlineOfTvOnClick: View,
            tvUnOnClick: TextView,
            underlineOfTvUnOnClick: View, context: Context
        ) {
            tvOnClick.setTextColor(context.getColor(R.color.white))
            tvUnOnClick.setTextColor(context.getColor(R.color.background_light))
            underlineOfTvOnClick.isVisible = true
            underlineOfTvUnOnClick.isVisible = false
        }
    }
}

class DiffCallBackContainerDataMovieHomeFra : DiffUtil.ItemCallback<ContainerDataMovieHomeFra>() {
    override fun areItemsTheSame(
        oldItem: ContainerDataMovieHomeFra,
        newItem: ContainerDataMovieHomeFra
    ): Boolean {
        return oldItem.type == newItem.type
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(
        oldItem: ContainerDataMovieHomeFra,
        newItem: ContainerDataMovieHomeFra
    ): Boolean {
        return oldItem == newItem
    }
}

class ContainerDataMovieHomeFra(
    val id: Int = 0,
    val type: Int = 0,
    val mListDataBannerMovie: ArrayList<TvShow> = arrayListOf(),
    val mListDataBodyMovie: ArrayList<TvShow> = arrayListOf(),
)