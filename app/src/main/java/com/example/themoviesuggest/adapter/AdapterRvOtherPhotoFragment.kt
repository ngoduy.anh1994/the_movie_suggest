package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviesuggest.databinding.LayoutItemRvOtherPhotoFragmentBinding

class AdapterRvOtherPhotoFragment: ListAdapter<OtherPhoto,ViewHolderOtherPhoto>(DiffCallBackOtherPhoto()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderOtherPhoto {
        return ViewHolderOtherPhoto(
            LayoutItemRvOtherPhotoFragmentBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolderOtherPhoto, position: Int) {
        holder.onBind(getItem(position))
    }

}
class ViewHolderOtherPhoto(private val binding: LayoutItemRvOtherPhotoFragmentBinding): RecyclerView.ViewHolder(binding.root){
    fun onBind(photo: OtherPhoto){
        itemView.run {
            Glide.with(this).load(photo.img).into(binding.ivItemPhoto)
        }
    }
}
class DiffCallBackOtherPhoto: DiffUtil.ItemCallback<OtherPhoto>(){
    override fun areItemsTheSame(oldItem: OtherPhoto, newItem: OtherPhoto): Boolean {
        return newItem == oldItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: OtherPhoto, newItem: OtherPhoto): Boolean {
        return newItem.img == oldItem.img
    }

}
class OtherPhoto(
    val img: String = ""
)