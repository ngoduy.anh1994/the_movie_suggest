package com.example.themoviesuggest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.LayoutItemRvInBodyHomeFraBinding
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain

class AdapterItemBodyInRvOfHomeFra(
    private val interfacePassData: InterfacePassData,
    private val fmActivity: FragmentActivity
) : ListAdapter<TvShow, AdapterItemBodyInRvOfHomeFra.ViewHolderItemBodyInRvOfHomeFra>(DiffCallBackItemBodyInRvOfHomeFra()){
    private val viewModelMain by lazy {
        ViewModelProvider(fmActivity,AppViewModelFactory(fmActivity))[ViewModelMain::class.java]
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolderItemBodyInRvOfHomeFra {
        return ViewHolderItemBodyInRvOfHomeFra(
            LayoutItemRvInBodyHomeFraBinding.inflate(LayoutInflater.from(parent.context),parent,false),interfacePassData
        )
    }

    override fun onBindViewHolder(holder: ViewHolderItemBodyInRvOfHomeFra, position: Int) {
        holder.onBind(getItem(position))
    }
    inner class ViewHolderItemBodyInRvOfHomeFra(private val binding : LayoutItemRvInBodyHomeFraBinding,private val interfacePassData: InterfacePassData) : RecyclerView.ViewHolder(binding.root){
        fun onBind(tvShow: TvShow){
            itemView.run {
                Glide.with(this).load(tvShow.image_thumbnail_path).into(binding.itemRvInBodyHomeFra.ivMovieItemRvInBodyHomeFra)
                binding.itemRvInBodyHomeFra.tvNameMovieItemRvInBodyHomeFra.text = tvShow.name
                binding.itemRvInBodyHomeFra.tvCountryItemRvInBodyHomeFra.text = tvShow.country
                binding.itemRvInBodyHomeFra.tvReleaseDateItemRvInBodyHomeFra.text = tvShow.start_date
                if (tvShow.save_movie){
                    binding.itemRvInBodyHomeFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                }else{
                    binding.itemRvInBodyHomeFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                }
                binding.itemRvInBodyHomeFra.ivSaveItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.save_movie = !tvShow.save_movie
                    if (tvShow.save_movie){
                        binding.itemRvInBodyHomeFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                        viewModelMain.insertTvShow(tvShow)
                    }else{
                        binding.itemRvInBodyHomeFra.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                        viewModelMain.deleteTvShow(tvShow)
                    }
                }

                if (tvShow.rate_movie){
                    binding.itemRvInBodyHomeFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                }else{
                    binding.itemRvInBodyHomeFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                }
                binding.itemRvInBodyHomeFra.ivStarItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.rate_movie = !tvShow.rate_movie
                    if (tvShow.rate_movie){
                        binding.itemRvInBodyHomeFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                    }else{
                        binding.itemRvInBodyHomeFra.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                    }
                    if (tvShow.save_movie){
                        viewModelMain.insertTvShow(tvShow)
                    }else{
                        Toast.makeText(context, "if you want rate the movie.First, you should save it ", Toast.LENGTH_SHORT).show()
                    }
                }
                setOnClickListener {
                    interfacePassData.passDataMovie(tvShow)
                }
            }
        }
    }

}

class DiffCallBackItemBodyInRvOfHomeFra : DiffUtil.ItemCallback<TvShow>(){
    override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem == oldItem
    }

    override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return newItem.save_movie == oldItem.save_movie && newItem.rate_movie == oldItem.rate_movie
    }

}