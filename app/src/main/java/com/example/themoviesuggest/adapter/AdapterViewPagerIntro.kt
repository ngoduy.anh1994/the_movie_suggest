package com.example.themoviesuggest.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.themoviesuggest.model.intro.Intro
import com.example.themoviesuggest.ui.fragment.introduce.ContainerIntroFragment

class AdapterViewPagerIntro(fm : FragmentActivity, private val listIntroduce : ArrayList<Intro>) : FragmentStateAdapter(fm) {
    override fun getItemCount(): Int = listIntroduce.size

    override fun createFragment(position: Int): Fragment {
        val dataIntro = listIntroduce[position]
        val containerIntroduce = ContainerIntroFragment()
        val bundle = Bundle()
        bundle.putSerializable("data_intro",dataIntro)
        containerIntroduce.arguments = bundle
        return containerIntroduce
    }

}