package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.LayoutRvSearchMovieBinding
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.main.SearchFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class AdapterRvSearchMovie(
    private val searchFragment: SearchFragment,
    private val interfacePassData: InterfacePassData
) : ListAdapter<TvShow, AdapterRvSearchMovie.SearchMovieViewHolder>(SearchMovieDiffCallBack()) {
    private val viewModelMain by lazy {
        ViewModelProvider(
            searchFragment,
            AppViewModelFactory(searchFragment.requireContext())
        )[ViewModelMain::class.java]
    }
    private var listDataUpdateTvShow: List<TvShow> = arrayListOf()
    fun receiverDataTvShowUpdate(data: List<TvShow>) {
        listDataUpdateTvShow = data
        notifyDataSetChanged()
    }

    inner class SearchMovieViewHolder(
        private val binding: LayoutRvSearchMovieBinding,
        private val interfacePassData: InterfacePassData
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun onBind(tvShow: TvShow) {
            itemView.run {
                Glide.with(this).load(tvShow.image_thumbnail_path)
                    .into(binding.itemRvSearch.ivMovieItemRvInBodyHomeFra)
                binding.itemRvSearch.tvNameMovieItemRvInBodyHomeFra.text = tvShow.name
                binding.itemRvSearch.tvCountryItemRvInBodyHomeFra.text = tvShow.country
                binding.itemRvSearch.tvReleaseDateItemRvInBodyHomeFra.text =
                    tvShow.start_date

                for (i in listDataUpdateTvShow.indices) {
                    for (j in 0 until currentList.size) {
                        if (currentList[j].id == listDataUpdateTvShow[i].id) {
                            currentList[j].save_movie = listDataUpdateTvShow[i].save_movie
                            currentList[j].rate_movie = listDataUpdateTvShow[i].rate_movie
                        }
                    }
                }

                if (tvShow.save_movie) {
                    binding.itemRvSearch.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)

                } else {
                    binding.itemRvSearch.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                }
                binding.itemRvSearch.ivSaveItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.save_movie = !tvShow.save_movie
                    if (tvShow.save_movie) {
                        viewModelMain.insertTvShow(tvShow)
                        binding.itemRvSearch.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_saved)
                    } else {
                        viewModelMain.deleteTvShow(tvShow)
                        binding.itemRvSearch.ivSaveItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_save)
                    }
                }

                if (tvShow.rate_movie) {
                    binding.itemRvSearch.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                } else {
                    binding.itemRvSearch.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                }
                binding.itemRvSearch.ivStarItemRvInBodyHomeFra.setOnClickListener {
                    tvShow.rate_movie = !tvShow.rate_movie
                    if (tvShow.rate_movie) {
                        binding.itemRvSearch.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                    } else {
                        binding.itemRvSearch.ivStarItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                    }
                    if (tvShow.save_movie) {
                        viewModelMain.insertTvShow(tvShow)
                    } else {
                        Toast.makeText(
                            context,
                            "if you want rate the movie.First, you should save it ",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                setOnClickListener {
                    interfacePassData.passDataMovie(tvShow)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchMovieViewHolder {
        return SearchMovieViewHolder(
            LayoutRvSearchMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            interfacePassData
        )
    }

    override fun onBindViewHolder(holder: SearchMovieViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }
}

class SearchMovieDiffCallBack : DiffUtil.ItemCallback<TvShow>() {
    override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return true
    }

    override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
        return oldItem == newItem
    }

}