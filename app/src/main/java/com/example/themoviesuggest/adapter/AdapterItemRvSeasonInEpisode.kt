package com.example.themoviesuggest.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.LayoutItemRvInSeasonEpisodeFragmentBinding
import com.example.themoviesuggest.interface_app.InterfaceShowSeason

class AdapterItemRvSeasonInEpisode(private val interfaceShowSeason: InterfaceShowSeason) :
    ListAdapter<NumberSeason, AdapterItemRvSeasonInEpisode.ViewHolderNumberSeason>(
        DiffCallBackNumberSeason()
    ) {
    inner class ViewHolderNumberSeason(
        private val binding: LayoutItemRvInSeasonEpisodeFragmentBinding,
        private val interfaceShowSeason: InterfaceShowSeason
    ) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun onBind(numberSeason: NumberSeason, position: Int) {
            itemView.run {
                binding.tvItemSeason.text = numberSeason.numberSeason.toString()
                var check = 0
                for (i in 0 until currentList.size) {
                    if (!currentList[i].mSelect) {
                        check++
                    }
                }
                if (check == currentList.size) {
                    currentList[0].mSelect = true
                }
                setOnClickListener {
                    interfaceShowSeason.showSeason(numberSeason)
                    for (i in 0 until currentList.size) {
                        if (currentList[i] != numberSeason && currentList[i].mSelect) {
                            currentList[i].mSelect = false
                        }
                        numberSeason.mSelect = true
                        notifyDataSetChanged()
                    }
                }
                if (numberSeason.mSelect) {
                    binding.ivBackgroundTvSeason.setImageResource(R.color.gray)
                } else {
                    binding.ivBackgroundTvSeason.setImageResource(R.color.black)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderNumberSeason {
        return ViewHolderNumberSeason(
            LayoutItemRvInSeasonEpisodeFragmentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),interfaceShowSeason
        )
    }

    override fun onBindViewHolder(holder: ViewHolderNumberSeason, position: Int) {
        holder.onBind(getItem(position), position)
    }
}

class DiffCallBackNumberSeason : DiffUtil.ItemCallback<NumberSeason>() {
    override fun areItemsTheSame(oldItem: NumberSeason, newItem: NumberSeason): Boolean {
        return oldItem.numberSeason == newItem.numberSeason
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: NumberSeason, newItem: NumberSeason): Boolean {
        return newItem == oldItem
    }

}

class NumberSeason(
    var numberSeason: Int = 1,
    var mSelect: Boolean = false
)