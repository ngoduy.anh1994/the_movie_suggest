package com.example.themoviesuggest.model.popular

data class DataMovieInHome(
    val page: Int = 0,
    val pages: Int = 0,
    val total: String = "",
    val tv_shows: ArrayList<TvShow> = arrayListOf()
)