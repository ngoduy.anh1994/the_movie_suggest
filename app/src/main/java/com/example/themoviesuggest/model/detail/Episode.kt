package com.example.themoviesuggest.model.detail

data class Episode(
    val air_date: String = "",
    val episode: Int = 0,
    val name: String = "",
    val season: Int = 0
)