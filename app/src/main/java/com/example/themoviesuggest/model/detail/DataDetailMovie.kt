package com.example.themoviesuggest.model.detail

data class DataDetailMovie(
    var tvShow: TvShow?= null
)