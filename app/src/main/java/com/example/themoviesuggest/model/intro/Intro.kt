package com.example.themoviesuggest.model.intro

import java.io.Serializable

data class Intro(var title : String = "" , var image : Int = 0) : Serializable