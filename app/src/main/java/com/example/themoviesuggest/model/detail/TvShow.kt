package com.example.themoviesuggest.model.detail

import java.io.Serializable

data class TvShow(
   // val countdown: Any,
    val country: String = "",
    val description: String = "",
    val description_source: String = "",
   // val end_date: Any,
    val episodes: ArrayList<Episode> = ArrayList<Episode>(),
    val genres: ArrayList<String> = ArrayList<String>(),
    val id: Int = 0,
    val image_path: String = "",
    val image_thumbnail_path: String = "",
    var name: String = "",
    val network: String = "",
    val permalink: String = "",
    val pictures: List<String> = ArrayList<String>(),
    val rating: String = "",
    val rating_count: String = "",
    val runtime: Int = 0,
    val start_date: String = "",
    val status: String = "",
    val url: String = "",
    //val youtube_link: Any
) : Serializable