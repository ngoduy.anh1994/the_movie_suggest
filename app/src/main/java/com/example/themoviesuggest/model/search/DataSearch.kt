package com.example.themoviesuggest.model.search

import com.example.themoviesuggest.model.popular.TvShow

data class DataSearch(
    val page: Int = 0,
    val pages: Int = 0,
    val total: String = "",
    val tv_shows: List<TvShow> = arrayListOf()
)