package com.example.themoviesuggest.constant

class ConstantOfApp {
    companion object{
        const val tag : String = "log"
        const val NAME_SHARED_PREFERENCE = "app_preference"
        const val CHECK_FIRST = "check_first"
        const val TYPE_DATA_MOVIE_AS_BANNER = 0
        const val TYPE_DATA_MOVIE_BODY = 1
        const val STATUS_RUNNING = "Running"
        const val STATUS_ENDED = "Ended"
        const val URL_API = "https://www.episodate.com/api/"
        const val STATE_MOVIE = "state movie"
        const val ID_MOVIE = "id data"
        const val MOVIE_STARTING_PAGING_INDEX = 1

        const val DATA_TVSHOW_POPULAR = "tv show"
        const val RETURN_SCREEN = "return"
        const val NUMBER_PAGE = "number page"
    }
}