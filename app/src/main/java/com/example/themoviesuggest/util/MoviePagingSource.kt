package com.example.themoviesuggest.util

import android.util.Log
import androidx.paging.PagingSource
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.model.popular.DataMovieInHome
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.retrofit.ApiServer
import com.example.themoviesuggest.retrofit.InstanceRetrofit

class MoviePagingSource(private val dataApi: ApiServer): PagingSource<Int,TvShow>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, TvShow> {
        return try {
            // Start refresh at page 1 if undefined.
            val position = params.key ?: ConstantOfApp.MOVIE_STARTING_PAGING_INDEX
            val response = dataApi.getDataMovieOfHomeFra(position)
            Log.e("position_size",position.toString())
            LoadResult.Page(
                data = response.tv_shows,
                prevKey = if (position == ConstantOfApp.MOVIE_STARTING_PAGING_INDEX) null else position - 1,
                nextKey = if (response.pages == 100) null else position+1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}