package com.example.themoviesuggest.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class Util {
    companion object{
        fun finishApp(view : View, activity: FragmentActivity){
            view.apply {
                setOnKeyListener { _, keyCode, _ ->
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        activity.finishAffinity()
                        true
                    } else {
                        false
                    }
                }
                isFocusableInTouchMode = true;
                requestFocus();
            }
        }

        fun createDialog(@LayoutRes layout: Int, context: Context, grayvity: Int): Dialog {
            val dialog = Dialog(context)
            dialog.apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                setCancelable(false)
                setCanceledOnTouchOutside(false)
                setContentView(layout)
            }.also { dialog ->
                val window: Window = dialog.window!!
                window.setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT
                )
                window.setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                )
                window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val windowAttribute = window.attributes
                windowAttribute.gravity = grayvity
                window.attributes = windowAttribute
                return dialog
            }
        }

        fun checkConnectInternet(context: Context) : Boolean{
            var connected = false
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            connected =
                connectivityManager!!.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)!!.state == NetworkInfo.State.CONNECTED ||
                        connectivityManager!!.getNetworkInfo(ConnectivityManager.TYPE_WIFI)!!.state == NetworkInfo.State.CONNECTED
            return connected
        }
    }
}