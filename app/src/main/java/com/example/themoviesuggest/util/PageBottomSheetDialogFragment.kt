package com.example.themoviesuggest.util

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.themoviesuggest.adapter.AdapterRvPageInBottomSheetDialog
import com.example.themoviesuggest.adapter.SelectPage
import com.example.themoviesuggest.interface_app.InterfacePage
import com.example.themoviesuggest.ui.fragment.main.CategoryFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


class PageBottomSheetDialogFragment(private val categoryFragment: CategoryFragment) : BottomSheetDialogFragment() {
    private lateinit var mAdapterPage: AdapterRvPageInBottomSheetDialog
    private lateinit var rvPage: RecyclerView
    private lateinit var ivClose: ImageView
    private var mListPage = ArrayList<SelectPage>()
    private val viewModelMain by lazy {
        ViewModelProvider(categoryFragment , AppViewModelFactory(requireContext()))[ViewModelMain::class.java]
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheet: BottomSheetDialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        val view = LayoutInflater.from(context).inflate(com.example.themoviesuggest.R.layout.layout_bottom_sheet_dialog_fragment_of_page, null)
        bottomSheet.setContentView(view)
        setupFullHeight(bottomSheet)
        mAdapterPage = AdapterRvPageInBottomSheetDialog(object : InterfacePage{
            override fun getPage(selectPage: SelectPage) {
                viewModelMain.apply {
                    getDataMovieBody(selectPage.page)
                    currentPage.value = selectPage.page
                }
            }
        },bottomSheet)
        viewModelMain.currentPage.onEach {
            mAdapterPage.getPageCurrent(it)
        }.launchIn(categoryFragment.lifecycleScope)
        // init view
        rvPage = view.findViewById(com.example.themoviesuggest.R.id.rv_bottom_sheet_dialog_of_page)
        ivClose = view.findViewById(com.example.themoviesuggest.R.id.iv_close_dialog_page)
        rvPage.apply {
            adapter = mAdapterPage
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL))
            scrollToPosition(viewModelMain.currentPage.value-3)
        }
        if (mListPage.isEmpty()){
            for (i in 1 .. 1000){
                mListPage.add(SelectPage(page = i, selectPage = false))
            }
        }

        mAdapterPage.submitList(mListPage)

        ivClose.setOnClickListener {
            bottomSheet.dismiss()
        }
        return bottomSheet
    }
    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet =
            bottomSheetDialog.findViewById<View>(com.example.themoviesuggest.R.id.design_bottom_sheet) as FrameLayout?
        val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(bottomSheet!!)
        val layoutParams = bottomSheet!!.layoutParams
        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            layoutParams.height = windowHeight
        }
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay.getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }
}