package com.example.themoviesuggest.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.themoviesuggest.util.Util
import kotlinx.coroutines.flow.MutableStateFlow

class AppBroadcastReceiver : BroadcastReceiver() {
    public val checkStateInternet by lazy {
        MutableStateFlow(false)
    }
    override fun onReceive(contex: Context?, p1: Intent?) {
        checkStateInternet.value = Util.checkConnectInternet(contex!!)
    }
}