package com.example.themoviesuggest.ui.activity

import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.ActivityIntroBinding
import com.example.themoviesuggest.ui.AppBroadcastReceiver

class MovieActivity : AppCompatActivity() {
    private lateinit var binding : ActivityIntroBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntroBinding.inflate(layoutInflater)
        setContentView(binding.root)
        findNavController(R.id.nav_host_introduce)
    }

}