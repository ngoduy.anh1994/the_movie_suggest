package com.example.themoviesuggest.ui.fragment.main

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterRvListWatchFragment
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentListWatchBinding
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.util.Util
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.w3c.dom.Text

class ListWatchFragment :
    BaseFragment<FragmentListWatchBinding>(FragmentListWatchBinding::inflate) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    private lateinit var diaLogSelect: Dialog
    private val viewModelMain by lazy {
        ViewModelProvider(
            requireActivity(),
            AppViewModelFactory(requireContext())
        )[ViewModelMain::class.java]
    }

    companion object {
        @JvmStatic
        fun newInstance() = ListWatchFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }
    private lateinit var mAdapterRvListWatchFragment: AdapterRvListWatchFragment
    private lateinit var ivCloseDialogFilter: ImageView
    private lateinit var swRate: SwitchCompat
    private lateinit var btnDone: MaterialButton
    private lateinit var swNotRate: SwitchCompat
    private var listRate: List<TvShow> = arrayListOf()
    private var listDone: List<TvShow> = arrayListOf()
    private var listNotRate: List<TvShow> = arrayListOf()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeApp()
        diaLogSelect = Dialog(requireContext())
        mAdapterRvListWatchFragment =
            AdapterRvListWatchFragment(this@ListWatchFragment, object : InterfacePassData {
                override fun passDataMovie(tvShow: TvShow) {
                    val bundle = Bundle()
                    bundle.putSerializable(ConstantOfApp.DATA_TVSHOW_POPULAR, tvShow)
                    requireActivity().findNavController(R.id.nav_host_introduce)
                        .navigate(R.id.detailFragment, bundle)
                }
            })
        binding.apply {
            rvListWatchFragment.adapter = mAdapterRvListWatchFragment
            rvListWatchFragment.layoutManager = LinearLayoutManager(context)
        }

        viewModelMain.apply {
            getDataTvShow()
        }
        diaLogSelect = Util.createDialog(
            R.layout.layout_dialog_item_switch_save_or_rate,
            requireContext(),
            Gravity.CENTER
        )
        ivCloseDialogFilter = diaLogSelect.findViewById(R.id.iv_close_dialog_select)
        swRate = diaLogSelect.findViewById(R.id.swOnOff_item_select_rate)
        btnDone = diaLogSelect.findViewById(R.id.btn_done)
        swNotRate = diaLogSelect.findViewById(R.id.swOnOff_item_select_not_rate)
        ivCloseDialogFilter.setOnClickListener {
            diaLogSelect.dismiss()
            diaLogSelect.cancel()
        }

        viewModelMain.apply {
            dataListTvShowFilter.onEach { listDataInRoom ->

                listNotRate = listDataInRoom.filter {
                    it.save_movie && !it.rate_movie
                }
                listRate = listDataInRoom.filter {
                    it.rate_movie && it.save_movie
                }

                listDone = if (dataSwRate.value && dataSwNotRate.value) {
                    listNotRate + listRate
                } else if (!dataSwRate.value && dataSwNotRate.value) {
                    listNotRate
                } else if (dataSwRate.value && !dataSwNotRate.value) {
                    listRate
                } else {
                    arrayListOf()
                }
                mAdapterRvListWatchFragment.submitList(listDone)
                binding.ivNoDataSave.isVisible = listDone.isEmpty()

                btnDone.setOnClickListener {
                    dataSwRate.value = swRate.isChecked
                    dataSwNotRate.value = swNotRate.isChecked
                    listDone = if (swRate.isChecked && swNotRate.isChecked) {
                        listNotRate + listRate
                    } else if (!swRate.isChecked && swNotRate.isChecked) {
                        listNotRate
                    } else if (swRate.isChecked && !swNotRate.isChecked) {
                        listRate
                    } else {
                        arrayListOf()
                    }
                    mAdapterRvListWatchFragment.submitList(listDone)

                    binding.ivNoDataSave.isVisible = listDone.isEmpty()
                    diaLogSelect.dismiss()
                }
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
        binding.ivSelectSaveOrRate.setOnClickListener {
            diaLogSelect.show()
            viewModelMain.apply {
                dataSwRate.onEach {
                    swRate.isChecked = it
                }.launchIn(this@ListWatchFragment.lifecycleScope)
                dataSwNotRate.onEach {
                    swNotRate.isChecked = it
                }.launchIn(this@ListWatchFragment.lifecycleScope)
            }
        }
    }
}