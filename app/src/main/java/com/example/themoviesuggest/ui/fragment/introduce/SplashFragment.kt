package com.example.themoviesuggest.ui.fragment.introduce

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.themoviesuggest.R
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentSplashBinding
import com.example.themoviesuggest.sharepreference.DoSharedPreference
import com.example.themoviesuggest.ui.AppBroadcastReceiver
import com.example.themoviesuggest.ui.fragment.BaseFragment
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlin.coroutines.CoroutineContext

class SplashFragment: BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeApp()
        appBroadcastReceiver.checkStateInternet.onEach {network->
            delay(1500)
            if (network) {
                if (!DoSharedPreference.getBoolean(ConstantOfApp.CHECK_FIRST)) {
                    findNavController().navigate(R.id.introFragment)
                } else {
                    gotoMain()
                }
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)

    }


}