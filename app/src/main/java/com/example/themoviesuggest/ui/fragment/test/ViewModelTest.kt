package com.example.themoviesuggest.ui.fragment.test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class ViewModelTest : ViewModel() {
    val data = MutableStateFlow<String>("")
    fun setData(s : String){
        viewModelScope.launch {
            data.value = s
        }
    }
}