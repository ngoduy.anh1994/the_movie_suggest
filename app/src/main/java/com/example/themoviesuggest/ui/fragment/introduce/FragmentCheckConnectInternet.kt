package com.example.themoviesuggest.ui.fragment.introduce

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.themoviesuggest.R
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentCheckConnectInternetBinding
import com.example.themoviesuggest.sharepreference.DoSharedPreference
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class FragmentCheckConnectInternet :
    BaseFragment<FragmentCheckConnectInternetBinding>(FragmentCheckConnectInternetBinding::inflate) {
    override fun onAttach(context: Context) {
        super.onAttach(context)
        closeApp()
    }
    @OptIn(DelicateCoroutinesApi::class)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        GlobalScope.launch(Dispatchers.Main) {
            if (!DoSharedPreference.getBoolean(ConstantOfApp.CHECK_FIRST)) {
                findNavController().navigate(R.id.introFragment)
            } else {
                delay(800)
                gotoMain()
            }
        }
    }
}
