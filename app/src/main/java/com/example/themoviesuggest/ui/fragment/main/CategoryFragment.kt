package com.example.themoviesuggest.ui.fragment.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterStateCategoryMovie
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentCategoryBinding
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.util.PageBottomSheetDialogFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class CategoryFragment : BaseFragment<FragmentCategoryBinding>(FragmentCategoryBinding::inflate) {
    companion object {
        @JvmStatic
        fun newInstance() = CategoryFragment()
    }
    private lateinit var mAppBottomSheetDialog: PageBottomSheetDialogFragment
    private lateinit var mAdapterStateCategoryMovie: AdapterStateCategoryMovie
    private val viewModelMain by lazy {
        ViewModelProvider(this, AppViewModelFactory(requireContext()))[ViewModelMain::class.java]
    }
    private var mListDataMovie = ArrayList<TvShow>()
    private var receiverStateMovie = ""
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    private fun hideOrShow(show: Boolean){
        if (show){
            binding.apply {
                ivBackStateMovieCategoryFra.isVisible = true
                tvStateMovieCategoryFra.isVisible = true
                containerPage.isVisible = true
                view3.isVisible = true
                rvStateMovieCategoryFra.isVisible = true
                progressBarLoadDataCategoryFragment.isVisible = false
            }
        }else{
            binding.apply {
                ivBackStateMovieCategoryFra.isVisible = false
                tvStateMovieCategoryFra.isVisible = false
                containerPage.isVisible = false
                view3.isVisible = false
                rvStateMovieCategoryFra.isVisible = false
                progressBarLoadDataCategoryFragment.isVisible = true
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mAdapterStateCategoryMovie = AdapterStateCategoryMovie(this,object : InterfacePassData{
            override fun passDataMovie(tvShow: TvShow) {
                val bundle = Bundle()
                bundle.putSerializable(ConstantOfApp.DATA_TVSHOW_POPULAR,tvShow)
                findNavController().navigate(R.id.detailFragment,bundle)
            }

        })
        receiverStateMovie = (arguments?.getString(ConstantOfApp.STATE_MOVIE).toString())
        binding.tvStateMovieCategoryFra.text = "State:  ${receiverStateMovie}"
        backToHome()
        mAppBottomSheetDialog = PageBottomSheetDialogFragment(this@CategoryFragment)
        binding.rvStateMovieCategoryFra.adapter = mAdapterStateCategoryMovie
        binding.rvStateMovieCategoryFra.layoutManager = LinearLayoutManager(context)
        viewModelMain.currentPage.onEach { currentPage ->
            binding.ivPrevPage.isVisible = currentPage != 1
            binding.ivNextPageCategoryFragment.isVisible = currentPage != 1000
        }.launchIn(this.lifecycleScope)
        binding.ivPrevPage.setOnClickListener {
            //  hideOrShow(false)
            viewModelMain.currentPage.value = viewModelMain.currentPage.value - 1
        }
        binding.ivNextPageCategoryFragment.setOnClickListener {
            //  hideOrShow(false)
            viewModelMain.currentPage.value = viewModelMain.currentPage.value + 1
        }
        binding.ivBackStateMovieCategoryFra.setOnClickListener {
            findNavController().popBackStack()
        }
        binding.containerPage.setOnClickListener {
            mAppBottomSheetDialog.show(parentFragmentManager,mAppBottomSheetDialog.tag)
        }
        addDataMovie()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun addDataMovie() {
        viewModelMain.apply {
            getDataTvShow()
            currentPage.onEach { currentPage ->
                binding.tvNumberPageCategoryFragment.text = currentPage.toString()
                getDataMovieBody(currentPage)
                hideOrShow(false)
            }.launchIn(this@CategoryFragment.lifecycleScope)
            dataMovie.onEach { dataMovieInHome ->
                hideOrShow(true)

                mListDataMovie = dataMovieInHome.tv_shows.filter {
                    it.status ==  receiverStateMovie
                } as ArrayList<TvShow>
                dataListTvShow.onEach {
                    for (i in it.indices){
                        for (j in 0 until mListDataMovie.size){
                            if (mListDataMovie[j].id == it[i].id){
                                mListDataMovie[j].save_movie = it[i].save_movie
                                mListDataMovie[j].rate_movie = it[i].rate_movie
                            }
                        }
                    }
                }.launchIn(this@CategoryFragment.lifecycleScope)
                binding.rvStateMovieCategoryFra.isVisible = true
                binding.progressBarLoadDataCategoryFragment.isVisible = false
                mAdapterStateCategoryMovie.submitList(mListDataMovie)
                mAdapterStateCategoryMovie.notifyDataSetChanged()
            }.launchIn(this@CategoryFragment.lifecycleScope)
        }
    }
}