package com.example.themoviesuggest.ui.fragment.test

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.FragmentOneBinding
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class FragmentOne : Fragment() {
    private val vm by lazy {
        ViewModelProvider(requireActivity())[ViewModelTest::class.java]
    }
    private lateinit var binding: FragmentOneBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOneBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val x = arguments?.getString("1")
        binding.edOne.setText(x)
        binding.btnNextTo2.setOnClickListener {
           val bundle = Bundle()
            bundle.putString("1",binding.edOne.text.toString())
            findNavController().navigate(R.id.fragmentTwo,bundle)
        }
    }

    override fun onStart() {
        super.onStart()

    }


}