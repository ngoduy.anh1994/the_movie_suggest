package com.example.themoviesuggest.ui.fragment.introduce

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.widget.ViewPager2
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterViewPagerIntro
import com.example.themoviesuggest.databinding.FragmentIntroBinding
import com.example.themoviesuggest.model.intro.Intro
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.util.Util
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class IntroFragment : BaseFragment<FragmentIntroBinding>(FragmentIntroBinding::inflate) {
    private lateinit var adapterViewPagerIntro: AdapterViewPagerIntro
    private var listIntroduce = ArrayList<Intro>()
    private lateinit var dialog: Dialog
    private lateinit var tvCloseApp: TextView
    @OptIn(DelicateCoroutinesApi::class)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeApp()
        listIntroduce = dataListIntroduce()
        dialog = Util.createDialog(
            R.layout.layout_dialog_error_un_connect_internet,
            requireActivity(),
            Gravity.CENTER
        )

        tvCloseApp = dialog.findViewById(R.id.tv_close_app_dialog_un_connect)
        adapterViewPagerIntro = AdapterViewPagerIntro(requireActivity(), listIntroduce)
        binding.vpIntroFra.adapter = adapterViewPagerIntro
        binding.circleIndicatorIntroFra.setViewPager(binding.vpIntroFra)
        binding.vpIntroFra.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                if (position == listIntroduce.size - 1) {
                    binding.btnNextToScreenHomeFra.isVisible = true
                    binding.btnSkipIntroFra.isVisible = false
                    binding.btnNextToScreenHomeFra.setOnClickListener {
                        gotoMain()
                    }
                } else {
                    binding.btnNextToScreenHomeFra.isVisible = false
                    binding.btnSkipIntroFra.isVisible = true
                }
            }
        })
        binding.btnSkipIntroFra.setOnClickListener {
            gotoMain()
        }
//        appBroadcastReceiver.checkStateInternet.onEach { network ->
//            if (network) {
//                dialog.dismiss()
//
//            } else {
//                delay(300)
//                dialog.show()
//                tvCloseApp.setOnClickListener {
//                    requireActivity().finishAffinity()
//                }
//            }
//        }.launchIn(viewLifecycleOwner.lifecycleScope)

    }

    private fun dataListIntroduce(): ArrayList<Intro> {
        return ArrayList<Intro>().apply {
            add(Intro("", R.drawable.iv_intro_1))
            add(Intro("", R.drawable.iv_intro_2))
            add(Intro("", R.drawable.iv_intro_3))
            add(Intro("", R.drawable.iv_intro_4))
        }
    }
}


