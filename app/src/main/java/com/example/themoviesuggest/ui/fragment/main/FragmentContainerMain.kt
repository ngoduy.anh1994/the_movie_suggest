package com.example.themoviesuggest.ui.fragment.main

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.themoviesuggest.R
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentContainerMainBinding
import com.example.themoviesuggest.sharepreference.DoSharedPreference
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.util.Util
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class FragmentContainerMain :
    BaseFragment<FragmentContainerMainBinding>(FragmentContainerMainBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeApp()

        binding.bottomNavMainAct.setupWithNavController(
            requireActivity().findNavController(R.id.nav_host_main)
        )
        binding.bottomNavMainAct.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.homeFragment -> requireActivity().findNavController(R.id.nav_host_main)
                    .navigate(R.id.homeFragment)
                R.id.searchFragment -> requireActivity().findNavController(R.id.nav_host_main)
                    .navigate(R.id.searchFragment)
                R.id.listWatchFragment -> requireActivity().findNavController(R.id.nav_host_main)
                    .navigate(R.id.listWatchFragment)
            }
            return@setOnItemSelectedListener true
        }
        DoSharedPreference.putBoolean(ConstantOfApp.CHECK_FIRST, true)
    }
}