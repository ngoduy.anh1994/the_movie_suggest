package com.example.themoviesuggest.ui.fragment.fragment_tablayout_detail_fra

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesuggest.adapter.*
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentEpisodeBinding
import com.example.themoviesuggest.interface_app.InterfaceShowSeason
import com.example.themoviesuggest.model.detail.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class EpisodeFragment : BaseFragment<FragmentEpisodeBinding>(FragmentEpisodeBinding::inflate) {
    private lateinit var dataTvShow: TvShow
    companion object {
        @JvmStatic
        fun newInstance(tvShow: TvShow) = EpisodeFragment().apply {
            dataTvShow = tvShow
        }
    }
    private var mSeason = ArrayList<Int>()
    private var showSeason: Int = 1
    val listenSeason = MutableStateFlow<Int>(1)
    private var mListSeason = ArrayList<NumberSeason>()
    private lateinit var mAdapterEpisode : AdapterItemRvEpisode
    private lateinit var mAdapterSeason: AdapterItemRvSeasonInEpisode
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapterEpisode = AdapterItemRvEpisode()
        mAdapterSeason = AdapterItemRvSeasonInEpisode(object :InterfaceShowSeason {
            override fun showSeason(numberSeason: NumberSeason) {
                showSeason = numberSeason.numberSeason
                listenSeason.value = showSeason
            }
        })
        binding.apply {
            // season
            rvItemSeasonInEpisodeFragment.adapter = mAdapterSeason
            rvItemSeasonInEpisodeFragment.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
            //episode
            rvItemEpisodeInEpisodeFragment.adapter = mAdapterEpisode
            rvItemEpisodeInEpisodeFragment.layoutManager = LinearLayoutManager(context)
        }
        addDataEpisode()
    }

    private fun addDataEpisode() {
        dataTvShow.episodes.forEach {
            if (!mSeason.contains(it.season)) {
                mSeason.add(it.season)
            }
        }
        mSeason.forEach {
            mListSeason.add(NumberSeason(it))
        }
        mAdapterSeason.submitList(mListSeason)

        listenSeason.onEach { season->
            val listDataEpisode = dataTvShow.episodes.filter {
                it.season == season
            }
            mAdapterEpisode.submitList(listDataEpisode)
            mAdapterEpisode.notifyDataSetChanged()
        }.launchIn(this@EpisodeFragment.lifecycleScope)

    }
}