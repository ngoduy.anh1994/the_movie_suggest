package com.example.themoviesuggest.ui.fragment.main

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterContainerRVHomeFra
import com.example.themoviesuggest.adapter.ContainerDataMovieHomeFra
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentHomeBinding
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.util.Util
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {
    private lateinit var mAdapterContainerRVHomeFra: AdapterContainerRVHomeFra
    private var mListContainerDataMovieHomeFra = ArrayList<ContainerDataMovieHomeFra>()
    private val viewModelHomeFra by lazy {
        ViewModelProvider(
            requireActivity(),
            AppViewModelFactory(requireContext())
        )[ViewModelMain::class.java]
    }
    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeApp()
        mAdapterContainerRVHomeFra = AdapterContainerRVHomeFra(object : InterfacePassData {
            override fun passDataMovie(tvShow: TvShow) {
                val bundle = Bundle()
                bundle.putSerializable(ConstantOfApp.DATA_TVSHOW_POPULAR, tvShow)
                requireActivity().findNavController(R.id.nav_host_introduce)
                    .navigate(R.id.detailFragment, bundle)
            }
        }, requireActivity())
        binding.rvContainerHomeFra.adapter = mAdapterContainerRVHomeFra
        binding.rvContainerHomeFra.layoutManager = LinearLayoutManager(requireActivity())
        mAdapterContainerRVHomeFra.submitList(mListContainerDataMovieHomeFra)
        binding.edSearchView.setOnClickListener {
            findNavController().navigate(R.id.searchFragment)
        }
    }

    override fun onStart() {
        super.onStart()
        getDataMovie()
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun getDataMovie() {
        viewModelHomeFra.apply {
            getDataMovieBody(1)
            dataMovie.onEach { dataMovie ->
                mListContainerDataMovieHomeFra.clear()
                if (dataMovie.tv_shows.isNotEmpty()){
                    mListContainerDataMovieHomeFra.add(
                        ContainerDataMovieHomeFra(
                            type = ConstantOfApp.TYPE_DATA_MOVIE_AS_BANNER,
                            mListDataBannerMovie = dataMovie.tv_shows,
                        )
                    )
                    mListContainerDataMovieHomeFra.add(
                        ContainerDataMovieHomeFra(
                            type = ConstantOfApp.TYPE_DATA_MOVIE_BODY,
                            mListDataBodyMovie = dataMovie.tv_shows,
                        )
                    )
                    mAdapterContainerRVHomeFra.submitList(mListContainerDataMovieHomeFra)
                    mAdapterContainerRVHomeFra.notifyDataSetChanged()
                }
            }.launchIn(requireActivity().lifecycleScope)
        }
    }
}