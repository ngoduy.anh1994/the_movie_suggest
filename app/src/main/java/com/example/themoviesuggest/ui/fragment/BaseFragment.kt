package com.example.themoviesuggest.ui.fragment

import android.app.Activity
import android.app.Dialog
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.example.themoviesuggest.R
import com.example.themoviesuggest.ui.AppBroadcastReceiver
import com.example.themoviesuggest.util.Util
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB: ViewBinding>(
    private val inflate: Inflate<VB>
) : Fragment() {
    private var _binding: VB? = null
    val binding get() = _binding!!
    protected lateinit var appBroadcastReceiver: AppBroadcastReceiver
    private lateinit var dialog: Dialog
    private lateinit var btnRetry: MaterialButton
    private lateinit var btnCancel: MaterialButton
    private lateinit var progressBar: ProgressBar
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appBroadcastReceiver = AppBroadcastReceiver()
        dialog = Util.createDialog(R.layout.layout_dialog_error_404, requireContext(), Gravity.CENTER)
        // view of dialog
        btnRetry = dialog.findViewById<MaterialButton>(R.id.btn_retry)
        btnCancel = dialog.findViewById<MaterialButton>(R.id.btn_cancel)
        progressBar = dialog.findViewById(R.id.progress_bar_load_connect_network)
        appBroadcastReceiver.checkStateInternet.onEach { network ->
            if (network) {
                dialog.dismiss()
                btnCancel.visibility = View.VISIBLE
                btnRetry.visibility = View.VISIBLE
                progressBar.isVisible = false

            } else {
                delay(200)
                dialog.show()
                btnCancel.setOnClickListener {
                    dialog.dismiss()
                }
                btnRetry.setOnClickListener {
                    if (!network) {
                        btnCancel.visibility = View.INVISIBLE
                        btnRetry.visibility = View.INVISIBLE
                        progressBar.isVisible = true
                    }
                }
            }

        }.launchIn(viewLifecycleOwner.lifecycleScope)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected fun closeApp(){
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                requireActivity().finishAffinity()
            }
        })
    }

    protected fun backToHome(){
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.fragmentContainerMain)
            }
        })
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        requireActivity().registerReceiver(appBroadcastReceiver, filter)
    }

    override fun onStop() {
        super.onStop()
        requireActivity().unregisterReceiver(appBroadcastReceiver);
    }

    protected fun gotoMain() {
        findNavController().navigate(R.id.fragmentContainerMain)
    }
    protected fun hideSoftKeyboard(activity: FragmentActivity) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(
                activity.currentFocus!!.windowToken,
                0
            )
        }
    }
}