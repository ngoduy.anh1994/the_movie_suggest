package com.example.themoviesuggest.ui.fragment.introduce

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.themoviesuggest.R
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentContainerIntroBinding
import com.example.themoviesuggest.model.intro.Intro
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.util.Util

class ContainerIntroFragment : BaseFragment<FragmentContainerIntroBinding>(FragmentContainerIntroBinding::inflate){
    private lateinit var receiverDataIntroduce : Intro
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeApp()
        try {
            receiverDataIntroduce = arguments?.get("data_intro") as Intro
            binding.itemTxtFraContainerIntro.text = receiverDataIntroduce.title
            Glide.with(this).load(receiverDataIntroduce.image).into(binding.itemImgFraContainerIntro)
        }catch (e : Exception){
            Log.e(ConstantOfApp.tag,"error data receive data intro null")
        }
    }

}