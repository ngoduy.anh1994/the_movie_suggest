package com.example.themoviesuggest.ui.fragment.main

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterRvSearchMovie
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentSearchBinding
import com.example.themoviesuggest.interface_app.InterfacePassData
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent.setEventListener
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener


class SearchFragment : BaseFragment<FragmentSearchBinding>(FragmentSearchBinding::inflate) {
    companion object {
        @JvmStatic
        fun newInstance() = SearchFragment()
    }

    private val viewModelMain by lazy {
        ViewModelProvider(this, AppViewModelFactory(requireContext()))[ViewModelMain::class.java]
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    private lateinit var mAdapSearchMovie: AdapterRvSearchMovie
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        closeApp()
        mAdapSearchMovie = AdapterRvSearchMovie(this@SearchFragment, object : InterfacePassData {
            override fun passDataMovie(tvShow: TvShow) {
                val bundle = Bundle()
                bundle.putSerializable(ConstantOfApp.DATA_TVSHOW_POPULAR, tvShow)
                requireActivity().findNavController(R.id.nav_host_introduce)
                    .navigate(R.id.detailFragment, bundle)
            }
        })
        binding.rvSearchFragment.adapter = mAdapSearchMovie
        binding.ivNoFindSearchFragment.isVisible = true
        binding.rvSearchFragment.layoutManager = LinearLayoutManager(context)
        binding.searchView.setOnQueryTextListener(object :
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                if (p0 != null) {
                    viewModelMain.getDataSearchMovie(p0)
                }
                hideSoftKeyboard(requireActivity())
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
//                if (p0 != null) {
//                    viewModelMain.getDataSearchMovie(p0)
//                }
                return false
            }
        })

        viewModelMain.apply {
            getDataTvShow()
            dataListTvShow.onEach {
                if (it.isNotEmpty()){
                    mAdapSearchMovie.receiverDataTvShowUpdate(it)
                }
            }.launchIn(this@SearchFragment.lifecycleScope)
            dataSearchMovie.onEach { dataSearch ->
                if (dataSearch.tv_shows.isNotEmpty()) {
                    binding.ivNoFindSearchFragment.isVisible = false
                    mAdapSearchMovie.submitList(dataSearch.tv_shows)
                }
            }.launchIn(this@SearchFragment.lifecycleScope)
        }

        binding.ivNoFindSearchFragment.setOnClickListener {
            hideSoftKeyboard(requireActivity())
        }


    }

}