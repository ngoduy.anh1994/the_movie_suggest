package com.example.themoviesuggest.ui.fragment.fragment_tablayout_detail_fra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterRvOtherPhotoFragment
import com.example.themoviesuggest.adapter.OtherPhoto
import com.example.themoviesuggest.databinding.FragmentOtherPhotoBinding
import com.example.themoviesuggest.model.detail.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment

class OtherPhotoFragment : BaseFragment<FragmentOtherPhotoBinding>(FragmentOtherPhotoBinding::inflate) {
    private lateinit var dataTvShow: TvShow
    companion object { @JvmStatic fun newInstance(tvShow: TvShow) = OtherPhotoFragment().apply {
        dataTvShow = tvShow
    } }
    private val listPhoto = ArrayList<OtherPhoto>()
    private val mAdapterRvOtherPhotoFragment = AdapterRvOtherPhotoFragment()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            rvOtherPhotoFragment.adapter = mAdapterRvOtherPhotoFragment
            rvOtherPhotoFragment.layoutManager = GridLayoutManager(context,2)
        }
        addDataPhoto()
    }

    private fun addDataPhoto() {
        dataTvShow.pictures.forEach {
            listPhoto.add(OtherPhoto(img = it))
        }
        mAdapterRvOtherPhotoFragment.submitList(listPhoto)
    }

}

