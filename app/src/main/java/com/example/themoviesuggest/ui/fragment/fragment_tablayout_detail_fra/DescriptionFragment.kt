package com.example.themoviesuggest.ui.fragment.fragment_tablayout_detail_fra

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.themoviesuggest.R
import com.example.themoviesuggest.databinding.FragmentDescriptionBinding
import com.example.themoviesuggest.model.detail.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment


class DescriptionFragment :
    BaseFragment<FragmentDescriptionBinding>(FragmentDescriptionBinding::inflate) {
    private lateinit var dataTvShow: TvShow

    companion object {
        @JvmStatic
        fun newInstance(tvShow: TvShow) = DescriptionFragment().apply {
            dataTvShow = tvShow
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            tvDescription.text = dataTvShow.description
            tvStartMovie.text = dataTvShow.start_date
            tvStatus.text = dataTvShow.status
            tvRating.text = dataTvShow.rating
            tvRateCount.text = dataTvShow.rating_count
            tvDuration.text = dataTvShow.runtime.toString() + "min"
        }

    }
}