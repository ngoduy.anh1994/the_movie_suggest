package com.example.themoviesuggest.ui.fragment.main

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.themoviesuggest.R
import com.example.themoviesuggest.adapter.AdapterItemRvGenresInRvInformationInDetailFragment
import com.example.themoviesuggest.adapter.AdapterViewpagerItemDetailMovieInRvDetailFragment
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.databinding.FragmentDetailBinding
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.ui.fragment.BaseFragment
import com.example.themoviesuggest.ui.fragment.fragment_tablayout_detail_fra.DescriptionFragment
import com.example.themoviesuggest.ui.fragment.fragment_tablayout_detail_fra.EpisodeFragment
import com.example.themoviesuggest.ui.fragment.fragment_tablayout_detail_fra.OtherPhotoFragment
import com.example.themoviesuggest.viewmodel.AppViewModelFactory
import com.example.themoviesuggest.viewmodel.ViewModelMain
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class DetailFragment : BaseFragment<FragmentDetailBinding>(FragmentDetailBinding::inflate) {
    private val viewModelMain by lazy {
        ViewModelProvider(this, AppViewModelFactory(requireContext()))[ViewModelMain::class.java]
    }
    private var receiverDataMovie: TvShow = TvShow()
    private var returnPage : Int = 0
    private var mAdapterItemRvGenresInRvInformationInDetailFragment: AdapterItemRvGenresInRvInformationInDetailFragment =
        AdapterItemRvGenresInRvInformationInDetailFragment()
    private lateinit var mAdapterViewpagerItemDetailMovieInRvDetailFragment: AdapterViewpagerItemDetailMovieInRvDetailFragment

    companion object {
        @JvmStatic
        fun newInstance() = DetailFragment()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.progressBarLoadMovieDetail.isVisible = true

        receiverDataMovie = arguments?.get(ConstantOfApp.DATA_TVSHOW_POPULAR) as TvShow
        binding.collapsingDetailFragment.setContentScrimColor(resources.getColor(R.color.background_of_app))
        binding.rvItemRvInBodyHomeFra.adapter =
            mAdapterItemRvGenresInRvInformationInDetailFragment
        binding.rvItemRvInBodyHomeFra.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        backToScreen()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onStart() {
        super.onStart()
        viewModelMain.apply {
            getDataDetailMovie(receiverDataMovie.id)
            checkCallApi.onEach { checkCallApi ->
                if (checkCallApi) {
                    dataDetailMovie.onEach { dataDetailMovie ->
                        if (dataDetailMovie.tvShow != null) {
                            binding.apply {
                                progressBarLoadMovieDetail.isVisible = false
                                appBarDetailFragment.isVisible = true
                                vpDetailFragment.isVisible = true
                                tabLayoutItemDetailMovieInRvDetailFra.isVisible = true
                            }
                            if (dataDetailMovie.tvShow!!.pictures.isEmpty()) {
                                Glide.with(this@DetailFragment)
                                    .load("https://static.vecteezy.com/system/resources/previews/004/141/669/non_2x/no-photo-or-blank-image-icon-loading-images-or-missing-image-mark-image-not-available-or-image-coming-soon-sign-simple-nature-silhouette-in-frame-isolated-illustration-vector.jpg")
                                    .into(binding.ivIntroDetailMovie)
                            } else {
                                Glide.with(this@DetailFragment)
                                    .load(dataDetailMovie.tvShow!!.pictures[0])
                                    .into(binding.ivIntroDetailMovie)
                            }

                            Glide.with(this@DetailFragment)
                                .load(dataDetailMovie.tvShow!!.image_path)
                                .into(binding.ivMovieDetailMovie)
                            binding.tvNameMovieItemRvInBodyHomeFra.text =
                                dataDetailMovie.tvShow!!.name
                            binding.tvPointRateMovieItemRvInBodyHomeFra.text =
                                dataDetailMovie.tvShow!!.rating_count

                            mAdapterItemRvGenresInRvInformationInDetailFragment.receiverData(
                                dataDetailMovie.tvShow!!.genres
                            )

                            //iv save
                            if (receiverDataMovie.save_movie) {
                                binding.ivSaveDetailMovie.setImageResource(R.drawable.ic_saved)
                            } else {
                                binding.ivSaveDetailMovie.setImageResource(R.drawable.ic_un_save)
                            }
                            binding.ivSaveDetailMovie.setOnClickListener {
                                receiverDataMovie.save_movie = !receiverDataMovie.save_movie
                                if (receiverDataMovie.save_movie) {
                                    viewModelMain.insertTvShow(receiverDataMovie)
                                    binding.ivSaveDetailMovie.setImageResource(R.drawable.ic_saved)
                                } else {
                                    viewModelMain.deleteTvShow(receiverDataMovie)
                                    binding.ivSaveDetailMovie.setImageResource(R.drawable.ic_un_save)
                                }

                            }

                            //rate
                            if (receiverDataMovie.rate_movie) {
                                binding.ivRateMovieItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                            } else {
                                binding.ivRateMovieItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                            }
                            binding.ivRateMovieItemRvInBodyHomeFra.setOnClickListener {
                                receiverDataMovie.rate_movie = !receiverDataMovie.rate_movie
                                if (receiverDataMovie.rate_movie) {
                                    binding.ivRateMovieItemRvInBodyHomeFra.setImageResource(R.drawable.ic_stared)
                                } else {
                                    binding.ivRateMovieItemRvInBodyHomeFra.setImageResource(R.drawable.ic_un_star)
                                }
                                if (receiverDataMovie.save_movie) {
                                    viewModelMain.insertTvShow(receiverDataMovie)
                                }else{
                                    Toast.makeText(context, "if you want rate the movie.First, you should save it ", Toast.LENGTH_SHORT).show()
                                }

                            }


                            mAdapterViewpagerItemDetailMovieInRvDetailFragment =
                                AdapterViewpagerItemDetailMovieInRvDetailFragment(
                                    childFragmentManager,
                                    viewLifecycleOwner.lifecycle,
                                    ArrayList<Fragment>().apply {
                                        add(DescriptionFragment.newInstance(dataDetailMovie.tvShow!!))
                                        add(EpisodeFragment.newInstance(dataDetailMovie.tvShow!!))
                                        add(OtherPhotoFragment.newInstance(dataDetailMovie.tvShow!!))
                                    })
                            binding.vpDetailFragment.adapter =
                                mAdapterViewpagerItemDetailMovieInRvDetailFragment

                            TabLayoutMediator(
                                binding.tabLayoutItemDetailMovieInRvDetailFra,
                                binding.vpDetailFragment
                            ) { tab, position ->
                                when (position) {
                                    0 -> tab.text = "About Movie"
                                    1 -> tab.text = "Episode"
                                    2 -> tab.text = "Other Photo"
                                }
                                binding.vpDetailFragment.setCurrentItem(tab.position, true)
                            }.attach()
                            binding.vpDetailFragment.isUserInputEnabled = false
                            binding.ivBackDetailMovie.setOnClickListener {
                                findNavController().popBackStack()
                            }
                        }
                    }.launchIn(viewLifecycleOwner.lifecycleScope)
                }
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    private fun backToScreen() {
        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                    findNavController().popBackStack()
            }


        })
    }
}