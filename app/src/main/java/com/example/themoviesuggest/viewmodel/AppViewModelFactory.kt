package com.example.themoviesuggest.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AppViewModelFactory(private val context: Context): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        with(modelClass) {
            when {
                isAssignableFrom(ViewModelMain::class.java) -> {
                    ViewModelMain(context)
                }
                else -> throw IllegalStateException("unknown view model: $modelClass")
            }
        } as T

}