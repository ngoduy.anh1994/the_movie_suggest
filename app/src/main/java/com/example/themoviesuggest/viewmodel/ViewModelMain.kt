package com.example.themoviesuggest.viewmodel

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.themoviesuggest.constant.ConstantOfApp
import com.example.themoviesuggest.model.detail.DataDetailMovie
import com.example.themoviesuggest.model.popular.DataMovieInHome
import com.example.themoviesuggest.model.popular.TvShow
import com.example.themoviesuggest.model.search.DataSearch
import com.example.themoviesuggest.retrofit.InstanceRetrofit
import com.example.themoviesuggest.room.AppDatabaseRoom
import com.example.themoviesuggest.ui.fragment.main.HomeFragment
import com.example.themoviesuggest.util.MoviePagingSource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import okhttp3.internal.wait

class ViewModelMain(context: Context) : ViewModel() {
    val dataMovie: MutableStateFlow<DataMovieInHome> = MutableStateFlow(DataMovieInHome())
    private val dataMovieInHomeFromApi = InstanceRetrofit.createInstanceRetrofitDataInHome()
    val dataDetailMovie: MutableStateFlow<DataDetailMovie> = MutableStateFlow(DataDetailMovie())
    val checkCallApi: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private var checkData = 0
    val currentPage = MutableStateFlow(1)
    val dataBase = AppDatabaseRoom.getInstance(context.applicationContext)!!.getAppDao()
    var dataListTvShow: Flow<List<TvShow>> = MutableStateFlow(arrayListOf())
    var dataListTvShowFilter: Flow<List<TvShow>> = MutableStateFlow(arrayListOf())

    var dataSwNotRate = MutableStateFlow(true)
    var dataSwRate = MutableStateFlow(true)
    val query = MutableStateFlow("")
    var dataSearchMovie = MutableStateFlow(DataSearch())
    fun getDataMovieBody(page: Int) {
        viewModelScope.launch {
            dataMovie.value = dataMovieInHomeFromApi.getDataMovieOfHomeFra(page)
        }
    }
    fun getDataDetailMovie(id: Int) {
        viewModelScope.launch {
            dataDetailMovie.value = dataMovieInHomeFromApi.getDataMovieDetail(id)
            if (dataDetailMovie.value.tvShow != null) {
                checkData = 1
            }
            checkCallApi.value = checkData == 1
        }
    }

    fun getDataSearchMovie(name: String) = viewModelScope.launch {
        dataSearchMovie.value = dataMovieInHomeFromApi.getDataMovieSearch(name)
    }

    fun insertTvShow(tvShow: TvShow) = viewModelScope.launch {
        dataBase.insertTvShow(tvShow)
    }

    fun deleteTvShow(tvShow: TvShow) = viewModelScope.launch {
        dataBase.deleteTvShow(tvShow)
    }

    fun getDataTvShow() {
        dataListTvShow = dataBase.getTvShow()
        dataListTvShowFilter = dataBase.getTvShow()
    }

}
