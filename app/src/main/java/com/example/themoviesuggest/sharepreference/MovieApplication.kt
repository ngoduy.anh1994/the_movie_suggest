package com.example.themoviesuggest.sharepreference

import android.app.Application

class MovieApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DoSharedPreference.init(applicationContext)
    }
}