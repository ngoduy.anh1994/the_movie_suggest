package com.example.themoviesuggest.retrofit

import com.example.themoviesuggest.constant.ConstantOfApp
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class InstanceRetrofit {
    companion object{
        fun createInstanceRetrofitDataInHome() : ApiServer{
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES) // write timeout
                .readTimeout(2, TimeUnit.MINUTES) // read timeout
                .addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .baseUrl(ConstantOfApp.URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiServer::class.java)
        }
    }
}