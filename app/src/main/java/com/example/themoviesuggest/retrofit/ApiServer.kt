package com.example.themoviesuggest.retrofit

import com.example.themoviesuggest.model.detail.DataDetailMovie
import com.example.themoviesuggest.model.popular.DataMovieInHome
import com.example.themoviesuggest.model.search.DataSearch
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServer {
    @GET("most-popular")
    suspend fun getDataMovieOfHomeFra(@Query("page") page : Int) : DataMovieInHome

    @GET("show-details")
    suspend fun getDataMovieDetail(@Query("q") id : Int) : DataDetailMovie

    @GET("search")
    suspend fun getDataMovieSearch(@Query("q") name: String,@Query("page") page: Int = 1): DataSearch
}