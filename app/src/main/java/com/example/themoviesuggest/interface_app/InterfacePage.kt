package com.example.themoviesuggest.interface_app

import com.example.themoviesuggest.adapter.SelectPage

interface InterfacePage {
    fun getPage(selectPage: SelectPage)
}