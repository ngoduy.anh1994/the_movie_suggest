package com.example.themoviesuggest.interface_app

import com.example.themoviesuggest.model.popular.TvShow
interface InterfacePassData {
    fun passDataMovie(tvShow: TvShow)
}