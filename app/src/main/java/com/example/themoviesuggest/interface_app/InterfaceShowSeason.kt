package com.example.themoviesuggest.interface_app

import com.example.themoviesuggest.adapter.NumberSeason

interface InterfaceShowSeason {
    fun showSeason(numberSeason: NumberSeason)
}