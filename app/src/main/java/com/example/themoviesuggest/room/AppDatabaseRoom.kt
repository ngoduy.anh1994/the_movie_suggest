package com.example.themoviesuggest.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.themoviesuggest.model.popular.TvShow


@Database(entities = [TvShow::class], version = 1)
abstract class AppDatabaseRoom: RoomDatabase() {
    abstract fun getAppDao(): AppDao
    companion object {
        @Volatile
        private var sUserDatabase: AppDatabaseRoom? = null
        const val DATABASE_NAME = "Room-database"
        @Synchronized
        fun getInstance(context: Context?): AppDatabaseRoom? {
            if (sUserDatabase == null) {
                sUserDatabase = Room.databaseBuilder(
                    context!!,
                    AppDatabaseRoom::class.java, DATABASE_NAME
                )
                   .allowMainThreadQueries()
                    .build()
            }
            return sUserDatabase
        }
    }
}
