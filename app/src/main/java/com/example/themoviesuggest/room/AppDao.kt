package com.example.themoviesuggest.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.themoviesuggest.model.popular.TvShow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

@Dao
interface AppDao {

    @Insert(onConflict = REPLACE)
    suspend fun insertTvShow(tvShow: TvShow)

    @Query("select * from tvshow order by id")
    fun getTvShow() : Flow<List<TvShow>>

    @Delete
    suspend fun deleteTvShow(tvShow: TvShow)
}